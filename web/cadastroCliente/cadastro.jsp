
<%@page import="br.com.senac.modelo.Cliente"%>
<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<jsp:include page="../header.jsp" />

<%
    CidadeDAO cidadeDAO = new CidadeDAO();

    List<Cidade> listaCidade = cidadeDAO.listarTodos();

%>

<script type="text/javascript">

    function buscaPais() {

        /*
         var dropdownCidade = document.getElementById('cidade') ; 
         var codigo = dropdownCidade.value;
         alert(codigo);
         */

        //utilizando Jquery
        var codigo = $('#cidade').val();

        if (codigo !== 0) {
            //vai buscar ....

            $.get(
                    "/Sakila/cadastroCidade/cidade.do",
                    {
                        'codigo': codigo
                    },
                    function (data) {
                        $('#pais').val(data);

                    });

        } else {
            //limpa o campo pais
            $('#pais').val('');

        }


    }

</script>

<div class="container">
    <fieldset>
        <body background="https://www.google.com/.png">
        <legend>Cadastro de Clientes</legend>
        <form class="form-horizontal" action="./cliente.do" method="post">

            <input type="hidden" name="codigo" value="<%= request.getAttribute("cliente") == null ? "0" : request.getParameter("codigo")  %>" />

            <div class="panel panel-default">
                <div class="panel-heading">
                    
                    <h3 class="panel-title"><font color="0000FF"><b>Dados Pessoais</b></font></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                        <div class="col-sm-2">
                            <input readonly="true" type="text" class="form-control" id="codigo"  value="<%= request.getAttribute("cliente") == null ? "" : ((Cliente)request.getAttribute("cliente")).getCodigo()  %>"   >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="primeiroNome"  >Primeiro nome:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="primeiroNome" placeholder="Entre com o primeiro nome" name="primeiroNome" required="true" >
                        </div>
                        <label class="control-label col-sm-2" for="ultimoNome">Ultimo nome:</label>
                        <div class="col-sm-4">
                            <input   type="text" class="form-control" id="ultimoNome" placeholder="Entre com o Ultimo nome" name="ultimoNome">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">E-mail:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="email" placeholder="Entre com o e-mail" name="email">
                        </div>
                        <label class="control-label col-sm-2" for="telefone">Telefone:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="telefone" placeholder="Entre com o telefone" name="telefone">
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="0000FF"><b>Endere�o</b></font></h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="cep">CEP:</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="cep" placeholder="Entre com o e-mail" name="email">
                        </div>
                        <label class="control-label col-sm-1" for="endereco">Endere�o:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="endereco" placeholder="Entre com o endere�o" name="endereco">
                        </div>
                        <label class="control-label col-sm-1" for="distrito">Distrito:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="distrito" placeholder="Entre com o distrito" name="distrito">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="complemento">Complemento:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="complemento" placeholder="Entre com o complemento" name="complemento">
                        </div>
                        <label class="control-label col-sm-1" for="cidade">Cidade:</label>
                        <div class="col-sm-2">
                            <select class="form-control " id="cidade" name="cidade" onchange="buscaPais()">
                                <option value="0">Selecione...</option>

                                <% for (Cidade c : listaCidade) {%>

                                <option value="<%= c.getCodigo()%>" ><%= c.getNome()%></option>

                                <% }%>

                            </select>
                        </div>
                        <label class="control-label col-sm-1" for="pais">Pa�s:</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="pais" name="pais" readonly="true">
                        </div>
                    </div>


                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-offset-10 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12"> 
                            <input type="submit"   class="btn btn-primary col-" value="Salvar" />
                            <span style="margin-left: 12px" />
                            <input type="reset"    class="btn btn-danger" value="Cancelar" />
                        </div>
                    </div>



                </div>
            </div>
        </form> 
    </body>                         
    </fieldset>


</div>



<jsp:include page="../footer.jsp" />