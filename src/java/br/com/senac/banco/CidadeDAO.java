package br.com.senac.banco;

import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Pais;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CidadeDAO implements DAO<Cidade> {

    @Override
    public void salvar(Cidade cidade) {
        Connection connection = null;
        try {

            connection = Conexao.getConnection();
            
            //inciando transacao .....
            connection.setAutoCommit(false);

            
            //executo query ....
            String query = "INSERT INTO country(country) values(?) ; ";

            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, cidade.getPais().getNome());

            ps.executeUpdate();

            //pego as chaves geradas 
            ResultSet rs = ps.getGeneratedKeys();
            rs.first();

            int codigoPais = rs.getInt(1);

            
            
            //executo a query utilizando chave gerada ....
            query = "INSERT INTO CITY (CITY , COUNTRY_ID) VALUES (? , ?)";
            ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, cidade.getNome());
            ps.setInt(2, codigoPais);

            ps.executeUpdate();
            
            rs = ps.getGeneratedKeys();
            rs.first();
            
            int codigoCidade = rs.getInt(1) ; 
            
            cidade.setCodigo(codigoCidade);
            cidade.getPais().setCodigo(codigoPais);
            

            connection.commit();

        } catch (Exception ex) {

            try {
                connection.rollback();
            } catch (SQLException ex1) {
                System.out.println("Falha ao desfazer transacao");
            }

            System.out.println("Falha ao salvar");

        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar conexao");
            }
        }
    }

    @Override
    public void atualizar(Cidade cidade) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletar(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Cidade> listarTodos() {
        List<Cidade> lista = new ArrayList<>();

        Connection connection = null;

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM city c inner join country co on c.country_ID = co.country_ID order by city asc; ";

            Statement st = connection.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {

                Pais pais = new Pais(rs.getInt("country_ID"), rs.getString("country"));

                Cidade cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"), pais);

                lista.add(cidade);
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return lista;
    }

    @Override
    public Cidade buscarPorId(int id) {

        Connection connection = null;
        Cidade cidade = null;

        try {
            connection = Conexao.getConnection();

            String query = "SELECT * FROM city c inner join country co "
                    + "on c.country_ID = co.country_ID where city_ID = ?  ; ";

            PreparedStatement ps = connection.prepareStatement(query);
            
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.first()) {

                Pais pais = new Pais(rs.getInt("country_ID"), rs.getString("country"));

                cidade = new Cidade(rs.getInt("city_ID"), rs.getString("city"), pais);
            }

        } catch (SQLException ex) {
            System.err.println("Erro ao atualizar ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Falha ao fechar o banco");
            }
        }

        return cidade;

    }

}
