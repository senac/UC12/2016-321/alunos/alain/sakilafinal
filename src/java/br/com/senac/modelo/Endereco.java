/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.modelo;

/**
 *
 * @author Administrador
 */
public class Endereco {

    private int codigo;
    private String cep;
    private String logradouro;
    private String distrito;
    private String complemento;
    private String telefone;
    private Cidade cidade;

    public Endereco() {
    }

    public Endereco(int codigo, String cep, String logradouro, String distrito, String complemento, String telefone, Cidade cidade) {
        this.codigo = codigo;
        this.cep = cep;
        this.logradouro = logradouro;
        this.distrito = distrito;
        this.complemento = complemento;
        this.telefone = telefone;
        this.cidade = cidade;
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return  "Cep: " +  cep +  " - " + logradouro + " , " +  distrito + " " + complemento +  cidade.getNome() +  " - " + cidade.getPais().getNome()  ;
    }

    
    
    

}
